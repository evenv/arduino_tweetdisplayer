#include <WiFi.h>
#include <WiFiClient.h>
#include <WiFiServer.h>
#include <WiFiUdp.h>

#include <LiquidCrystal.h>

LiquidCrystal lcd(9, 8, 5, 4, 3, 2);

char ssid[] = "Fort Vika"; //  your network SSID (name) 
char pass[] = "newyorknewyork";    // your network password (use for WPA, or use as key for WEP)
int status = WL_IDLE_STATUS;
byte server[] = { 23,23,245,47 }; // herokuapp

char inString[160]; // string for incoming webdata
int stringPos = 0; // string index counter
boolean startRead = false;

String tweet;
String hashtag;


int button1 = 6;
int button2 = 7;

// Initialize the Ethernet client library
// with the IP address and port of the server 
// that you want to connect to (port 80 is default for HTTP):
WiFiClient client;

const int dataDelay = 600000;

void setup() {
  //Initialize serial and wait for port to open:
  Serial.begin(9600); 
  Serial.println("Hello");
  // check for the presence of the shield:
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present"); 
    // don't continue:
    while(true);
  } 

  pinMode(button1, INPUT);
  pinMode(button2, INPUT);

   // Connect to wifi
  connectWiFi();

  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);
  // Print a message to the LCD.
  //lcd.print("Hello Amy Morris!");
  
  tweet = String("Starting...");
}

void loop() {
  
  Serial.println("New loop");
  
  int button1State = digitalRead(button1);
  int button2State = digitalRead(button2);
  
  if (button1State == HIGH) {
    hashtag = String("yolo");
    getTweet(tweet, hashtag);
  }
  else if (button2State == HIGH) {
    hashtag = String("wisdom");
    getTweet(tweet, hashtag);
  }
  
  String output = String(tweet + "                ");
  
  lcd.setCursor(16,0);
  lcd.autoscroll();
  for (int i = 0; i < output.length(); i++) {
    lcd.print( output.charAt(i) );
    delay(300);
  }
  lcd.noAutoscroll();
  lcd.clear();

  delay (500);

}

void getTweet(String &atweet, String &hashtag) {
  
  Serial.println("\nStarting connection to server...");
  // if you get a connection, report back via serial:
  if (client.connect(server, 80)) {
    Serial.println("connected to server");
    // Make a HTTP request:
    client.print("GET /pollhashtag/");
    client.print(hashtag);
    client.println(" HTTP/1.1");
    client.println("Host: tweetdisplayer.herokuapp.com");
    client.println("User-Agent: Arduino");
    client.println("Accept: text/html");
    client.println("Connection: close");
    client.println();

    atweet = readPage(); //go and read the output
    Serial.print(atweet);

  } else {
    atweet = String("connection failed");
    Serial.print("connection to server failed");
  }
}

void connectWiFi() {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:    
    status = WiFi.begin(ssid, pass);
  
    // wait 10 seconds for connection:
    delay(6000); 
    Serial.print("Status: ");
    Serial.println(status);
    Serial.println("Connected to wifi");
    printWifiStatus(); 
    
}

void disconnectWiFi() {
   status = WiFi.disconnect(); 
   Serial.println("Disconnected from wifi");
   Serial.print("Status: ");
   Serial.println(status);
}

void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

String readPage(){
  //read the page, and capture & return everything between '<' and '>'

  stringPos = 0;
  memset( &inString, 0, 160 ); //clear inString memory

  while(true){

    if (client.available()) {
      char c = client.read();

      if (c == '[' ) { //'[' is our begining character
        startRead = true; //Ready to start reading the part 
      }else if(startRead){

        if(c != ']'){ //']' is our ending character
          inString[stringPos] = c;
          stringPos ++;
        }else{
          //got what we need here! We can disconnect now
          startRead = false;
          client.stop();
          client.flush();
          Serial.println("disconnecting.");
          return inString;

        }

      }
    }

  }
 

}

